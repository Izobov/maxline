import React from 'react';
import { connect } from 'react-redux';
import r from './result.module.css'
import humidity from '../../img/humidity.png'
import temperature from '../../img/thermometer.png'
import wind from '../../img/wind.png'
import marker from '../../img/location-pin.png'


const Result = (props) => {

    if (props.data.length !== 0) {

        return <div className={r.wrapper}>
            <h1>Result:</h1>
            <div>

                <span>Today  in {props.data.name} is {props.data.weather[0].main}</span>
            </div>
            <div>
                <img src={humidity} alt="" />
                <span> humidity: {props.data.main.humidity}%</span>
            </div>
            <div>
                <img src={temperature} alt="" />
                <span> Temperature: {Math.ceil(props.data.main.temp - 273.15)} </span>
            </div>
            <div className={r.max_min}>

                <span> Max temperature: {Math.ceil(props.data.main.temp - 273.15)} </span>


                <span> Min temperature: {Math.ceil(props.data.main.temp - 273.15)} </span>
            </div>
            <div>
                <img src={wind} alt="" />
                <span> Wind speed: {props.data.wind.speed} m/s</span>
            </div>
            <div>
                <img src={marker} alt="" />
                <span> Coords: lon {props.data.coord.lon}, lat {props.data.coord.lat}</span>
            </div>
        </div>
    } else {
        return <div className={r.wrapper}>
            <h1>Result:</h1>
        </div>
    }

}

let mapStateToProps = (state) => ({
    data: state.result.weather
})



export default connect(mapStateToProps)(Result)