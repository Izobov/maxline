import React from 'react';
import Search from './Search';
import { connect } from 'react-redux';
import { updateText, setHelpers, setSearchHelpers } from '../../redux/reducers/search';
import { getSearch } from '../../redux/reducers/result';


const SearchContainer = (props) => {

    let parsHistory = JSON.parse(localStorage.getItem('history')) // тут получаем данные с localStorage



    let onChange = (event) => {
        let newText = event.target.value;
        props.updateText(newText)

        let result = [];
        //далее код для подсказок
        parsHistory.forEach(element => {  //для каждого элемента массива запускаем цикл и проверяем на совпадение

            let d = []
            for (let i = 0; i < newText.length; i++) {

                let a = element[i]
                let b = newText[i]

                console.log(d)
                if (a === b) {

                    if (!result.some(e => {

                        return e === element
                    })) {

                        result.push(element)   // если совпадают буквы, дообавит в переменную d
                    }

                } else {

                    if (result.some(e => {

                        return e === element
                    })) {
                        result.pop(element)
                    } // как только совпадение прекращается зануляет переменную и выходит из цикла
                }


            }

            console.log(d)
            // result.push(d[0])
        })

        console.log(result)

        return props.setSearchHelpers(result) // результат диспатчим 

    } // эта функция нужна для того, чтоб организовать FLUX структуру (UI поменяется только если меняется BLL )
    //P.S. можно было бы сделать и в локальный стейт но не стал заморачиваться

    let onClick = () => {
        props.getSearch(props.text) // вызывает thunk и устанавливает полученные данные с сервера в result_reducer

        let text = props.text
        if (parsHistory.length === 0) { //если в localStorage ничего нет, то добавит
            parsHistory.push(text)
        } else {   // если в localStorage уже что-то есть, то выполнит проверку, не совпадает ли текст с элементами что уже есть
            let some = parsHistory.some(element => {
                return element === text
            })
            if (!some) {     // если не совпал то добавит
                parsHistory.push(text)
            }

        }
        let jsonHistory = JSON.stringify(parsHistory) // по клику  поиска записываем новые данные в localStorage
        localStorage.setItem('history', jsonHistory) // }
        // props.setHelpers(parsHistory)

    }

    let elementClick = (event) => {

        let newText = event.currentTarget.innerText;
        props.updateText(newText)
    }

    return <Search text={props.text} onChange={onChange} onClick={onClick} helpers={props.searchHelpers} elementClick={elementClick} />
}

let mapStateToProps = (state) => ({
    text: state.search.text,
    history: state.search.history,
    searchHelpers: state.search.searchHelpers
})



export default connect(mapStateToProps, { updateText, getSearch, setHelpers, setSearchHelpers })(SearchContainer)