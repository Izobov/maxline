import React from 'react'
import s from './search.module.css'

const Search = (props) => {

    return <div className={s.wrapper}>
        <div className={s.search}>
            <input type="text" value={props.text} onChange={props.onChange} />
            <div className={s.helpArea}>
                {props.helpers.map(element =>
                    <span onClick={props.elementClick} className={s.helpers}>
                        {element}
                    </span>)}
            </div>
        </div>
        <div onClick={props.onClick} className={s.button}>Search</div>
    </div>
}

export default Search