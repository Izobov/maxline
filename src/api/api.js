import * as axios from 'axios';

let instance = axios.create({

    baseURL: 'https:/api.openweathermap.org/data/2.5',
})  // Настройки ajax запроса

const searchAPI = {
    getWeather(location) {
        return instance.get(`/weather?q=${location}&appid=e398b608eed1da1ac9edd657f2d905e3`).then(response => { return response.data })

    }
}

export default searchAPI