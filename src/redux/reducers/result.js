import searchAPI from "../../api/api"

const SET_WEATHER = 'SET_WEATHER'

let InitialState = {
    weather: []
}

const result_reducer = (state = InitialState, action) => {
    switch (action.type) {
        case SET_WEATHER:
            return {
                ...state,
                weather: action.weather
            }

        default: return state
    }
}

const setWeather = (weather) => ({ type: SET_WEATHER, weather })

export const getSearch = (location) => {
    return (dispatch) => {
        searchAPI.getWeather(location).then(response => {
            dispatch(setWeather(response))
        })
    }
}



export default result_reducer;