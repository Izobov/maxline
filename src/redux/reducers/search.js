

const TEXT_CHANGE = 'TEXT_CHANGE'
const SET_HELPERS = 'SET_HELPERS'
const SET_SEARCH_HELPERS = 'SET_SEARCH_HELPERS'

let InitialState = {
    text: "",
    history: [],
    searchHelpers: []
}

const search_reducer = (state = InitialState, action) => {
    switch (action.type) {
        case TEXT_CHANGE:
            return {
                ...state,
                text: action.newText
            }
        case SET_HELPERS:
            return {
                ...state,
                history: action.history
            }

        case SET_SEARCH_HELPERS: {
            return {
                ...state,
                searchHelpers: action.searchHelpers
            }
        }
        default: return state
    }
}

export const updateText = (newText) => ({ type: TEXT_CHANGE, newText })
export const setHelpers = (history) => ({ type: SET_HELPERS, history })
export const setSearchHelpers = (searchHelpers) => ({ type: SET_SEARCH_HELPERS, searchHelpers })




export default search_reducer