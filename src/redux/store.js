import { combineReducers, createStore, applyMiddleware } from "redux";
import search_reducer from "./reducers/search";
import thunkMidleware from 'redux-thunk';
import result_reducer from "./reducers/result";



let reducers = combineReducers({
    search: search_reducer,
    result: result_reducer

})

let store = createStore(reducers, applyMiddleware(thunkMidleware))

export default store

window.store = store