import React from 'react';
import './App.css';
import SearchContainer from './components/Search/SearchContainer';
import Result from './components/Result/Result';

function App() {
  if (!localStorage.getItem('history')) {
    let value = [];
    let jsonValue = JSON.stringify(value)
    localStorage.setItem('history', jsonValue)
  }
  return <div className="app_wrapper">
    <SearchContainer />
    <Result />
  </div>
}

export default App;
